package Utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitTypes {
	WebDriver driver;
	
	public WaitTypes (WebDriver driver) {
		 this.driver =driver;
		 
	}

	public WebElement waitForElement(By locator, int timeout ) {
		WebElement element = null;
		
		try {
			System.out.println("Waiting of maximum: " + timeout + " seconds for element to be availabe"); 
			
			WebDriverWait wait = new WebDriverWait(driver, 3);
			element = wait.until(
					ExpectedConditions.visibilityOfElementLocated(locator));
			System.out.println("Element appeard on the web page");
			
		} catch (Exception e) {
			System.out.println("Element not appeard");
		}
		return element;
		
	}
	public void ClickWhenReady(By locator, int timeout ) {
		
		try {
			WebElement element = null;
			System.out.println("Waiting of maximum: " + timeout + " seconds for element to be clickable"); 
			
			WebDriverWait wait = new WebDriverWait(driver, 3);
			element = wait.until(
					ExpectedConditions.elementToBeClickable(locator));
			element.click();
			System.out.println("Element clicked on the webpage");
			
		} catch (Exception e) {
			System.out.println("Element not appeard");
		}
		
		
	}
}
