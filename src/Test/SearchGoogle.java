package Test;



import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SearchGoogle {
	private WebDriver driver;
	private String baseUrl;

	@Before
	public void setUp() throws Exception {
		driver = new ChromeDriver();
		baseUrl = "https://www.google.hu/";

		// Maximize the browser's window
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Test
	public void testAutocomplete() throws Exception {
		driver.get(baseUrl);
		 driver.findElement(By.xpath("/html//input[@id='lst-ib']")).sendKeys("youtube");
		 driver.findElement(By.xpath("/html//div[@id='sbtc']/div[@class='gstl_0 sbdd_a']//ul[@role='listbox']/li[@class='gsfs']//input[@value='Google-keres�s']")).click();
		driver.findElement(By.xpath(".//*[@id='rso']/div[1]/div/div/div/h3/a")).click();
		
		Thread.sleep(2000);
		
		driver.findElement(By.xpath(".//*[@id='search']")).sendKeys("hands up mix");
		driver.findElement(By.xpath(".//*[@id='search-icon-legacy']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(".//*[@id='video-title']")).click();
	}
		
	

	@After
	public void tearDown() throws Exception {
		//driver.quit();
	}
}
