package Test;


import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import Page.classes.GmailSource;

public class Gmail {
	private WebDriver driver;
	private String baseUrl;

	@Before
	public void setUp() throws Exception {
		driver = new ChromeDriver();
		baseUrl = "https://www.gmail.com/";
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Test
	public void test() throws Exception {
		driver.get(baseUrl);
		GmailSource.EmailTextBox(driver).sendKeys("oltdavid");
		GmailSource.EmailNextButton(driver).click();
		Thread.sleep(2000);
		GmailSource.PasswordTextBox(driver).sendKeys("tamron990");
		Thread.sleep(2000);
		GmailSource.PasswordNextButton(driver).click();
		Thread.sleep(2000);
		GmailSource.WriteButton(driver).click();
		GmailSource.AdressText(driver).sendKeys("krisztinapalinko@gmail.com");
		
		
		
	}
	
	@After
	public void tearDown() throws Exception {
	}

}

//comment