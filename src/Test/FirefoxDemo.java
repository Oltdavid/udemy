package Test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxDemo {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver;
		System.setProperty("webdriver.gecko.driver", "C:\\Eclipse munka\\Geckodriver\\0.11.1\\geckodriver.exe");
		driver = new FirefoxDriver();
		String baseURL = "https://letskodeit.teachable.com/p/practice";

		driver.manage().window().maximize();
		driver.get(baseURL);

		driver.findElement(By.xpath(".//*[@id='name']")).sendKeys("D�vid");
		//Thread.sleep(5000);
		driver.findElement(By.xpath("/html//input[@id='confirmbtn']")).click();

	}

}

/*
 * package basicweb;
 * 
 * import org.openqa.selenium.By; import org.openqa.selenium.WebDriver; import
 * org.openqa.selenium.firefox.FirefoxDriver;
 * 
 * public class IdXPATHDemo {
 * 
 * public static void main(String[] args) { WebDriver driver; driver = new
 * FirefoxDriver(); String baseURL = "http://www.google.com";
 * driver.manage().window().maximize(); driver.get(baseURL);
 * 
 * driver.findElement(By.id("lst-ib")).sendKeys("letskodeit");
 * driver.findElement(By.xpath("//*[@id='sblsbb']/button")).click(); }
 * 
 * }
 */