package Test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {
	private Calculator calculator;

	@Before
	public void setUp() throws Exception {
		calculator = new Calculator();
	}

	
	@Test
	public void testAddShouldAddNumbersWhenBothNumbersArePositive() {
		long result = calculator.add(1, 1);
		assertEquals(2, result);
	}
	
	@After
	public void tearDown() throws Exception {
	}

	

}
