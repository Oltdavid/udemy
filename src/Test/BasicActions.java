package Test;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;

public class BasicActions {
	WebDriver driver;

	String baseURL;

	@Before
	public void setUp() throws Exception {
		baseURL = "https://letskodeit.teachable.com/";
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	@Test
	public void test() {
		driver.get(baseURL);
		driver.findElement(By.xpath(".//*[@id='navbar']/div/div/div/ul/li[2]/a")).click();
		System.out.println("Clicked on Login");
		driver.findElement(By.id("user_email")).clear();
		driver.findElement(By.id("user_email")).sendKeys("oltdavid@gmail.com");
		driver.findElement(By.id("user_password")).sendKeys("test");
		driver.findElement(By.name("commit")).click();
		

	}

	@After
	public void tearDown() throws Exception {
		//driver.quit();
	}

}
