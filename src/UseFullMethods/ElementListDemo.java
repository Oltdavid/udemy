package UseFullMethods;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
import UseFullMethods.GenericMethodsOld;

public class ElementListDemo {
	private WebDriver driver;
	private String baseUrl;
	private GenericMethodsOld gm;

	@Before
	public void setUp() throws Exception {
		driver = new ChromeDriver();
		gm = new GenericMethodsOld(driver);
		

		baseUrl = "http://letskodeit.teachable.com/pages/practice";

		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	@Test
	public void testLetsKodeIt() throws InterruptedException {
		driver.get(baseUrl);
		
		List <WebElement> elementList = gm.getElementList(".//input[@type='text']", "xpath");
		int size = elementList.size();
		System.out.println("Size of the element list is: " + size);
		
		
		
	}


	@After
	public void tearDown() throws Exception {
		Thread.sleep(2000);
		driver.quit();
	}
}