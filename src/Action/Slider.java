package Action;


import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.interactions.Actions;

public class Slider {
	private WebDriver driver;
	private String baseUrl;

	@Before
	public void setUp() throws Exception {
		driver = new ChromeDriver();
		baseUrl = "https://www.youtube.com/watch?v=J75qGv797Zo";

		// Maximize the browser's window
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@Test
	public void testSliderActions() throws Exception {
		driver.get(baseUrl);
		//driver.switchTo().frame(0);
		Thread.sleep(3000);
		
		// Using the actions class
		WebElement element = driver.findElement(By.xpath(".//*[@id='movie_player']/div[25]/div[1]/div[1]/div[4]/div"));
		Actions action = new Actions(driver);
		action.dragAndDropBy(element, 100, 0).perform();
		Thread.sleep(3000);
		action.dragAndDropBy(element, 100, 0).perform();
		Thread.sleep(3000);
		action.dragAndDropBy(element, -100, 0).perform();
		Thread.sleep(3000);
		action.dragAndDropBy(element, -100, 0).perform();
		
	}

	@After
	public void tearDown() throws Exception {
	}
}

//komment