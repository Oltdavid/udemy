package Page.classes;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class GmailSource {
	public static WebElement element = null;

	public static WebElement EmailTextBox(WebDriver driver) {
		element = driver.findElement(By.id("identifierId"));
		return element;
	}

	public static WebElement EmailNextButton(WebDriver driver) {
		element = driver.findElement(By.id("identifierNext"));
		return element;
	}

	public static WebElement PasswordTextBox(WebDriver driver) {
		element = driver.findElement(By.xpath("/html//div[@id='password']//input[@name='password']"));
		return element;
	}

	public static WebElement PasswordNextButton(WebDriver driver) {
		element = driver.findElement(By.id("passwordNext"));
		return element;

	}
	public static WebElement WriteButton(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@id=':ir']//div[@role='button']"));
		return element;
	}
	
	public static WebElement AdressText(WebDriver driver) {
		element = driver.findElement(By.xpath("/html//input[@id=':ny']"));
		return element;
	}

}
