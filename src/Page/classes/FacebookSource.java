package Page.classes;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FacebookSource {
	public static WebElement element = null;

	public static WebElement Email(WebDriver driver) {
		element = driver.findElement(By.id("email"));
		return element;
	}
	
	public static WebElement Pass(WebDriver driver) {
		element = driver.findElement(By.id("pass"));
		return element;
	}
	
	public static WebElement Bejelentkezes(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[@id='facebook']//label[@id='loginbutton']/input[@value='Bejelentkezés']"));
		return element;
	}

}